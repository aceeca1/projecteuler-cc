#ifndef GRAPH_H
#define GRAPH_H

#include <unordered_map>
#include <vector>

template <class Edge>
class EdgeListGraph {
   public:
    std::vector<std::vector<Edge>> out;

    int getSizeV() const { return out.size(); }
    const std::vector<Edge>& outEdge(int n) const { return out[n]; }
};

class UnitCostEdge {
   public:
    int target;
    int cost() { return 1; }
};

class CostEdge {
   public:
    int target, cost_;
    int cost() { return cost_; }
};

template <class Vertex, class Edge, class HashVertex = std::hash<Vertex>>
class GraphBuilder {
   public:
    std::unordered_map<Vertex, int, HashVertex> v;
    EdgeListGraph<Edge>* graph;

    GraphBuilder(EdgeListGraph<Edge>* graph_) : graph(graph_) {}

    int addVertex() {
        graph->out.emplace_back(0);
        return graph->getSizeV() - 1;
    }

    int addVertex(const Vertex& label) {
        auto pair = v.emplace(label, graph->getSizeV());
        if (pair.second) return addVertex();
        return pair.first->second;
    }
};

#endif  // GRAPH_H
