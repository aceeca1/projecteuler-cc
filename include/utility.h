#ifndef UTILITY_H
#define UTILITY_H

#include <farmhash.h>
#include <string>
#include <vector>

template <class T>
inline T &assignMax(T &lhs, const T &rhs) {
    if (lhs < rhs) lhs = rhs;
    return lhs;
}

template <class T, class T1>
inline T &assignMax(T &lhs, const T &rhs, T1 &lhs1, const T1 &rhs1) {
    if (lhs < rhs) {
        lhs = rhs;
        lhs1 = rhs1;
    }
    return lhs;
}

template <class T>
inline T &assignMin(T &lhs, const T &rhs) {
    if (rhs < lhs) lhs = rhs;
    return lhs;
}

template <class T, class T1>
inline T &assignMin(T &lhs, const T &rhs, T1 &lhs1, const T1 &rhs1) {
    if (rhs < lhs) {
        lhs = rhs;
        lhs1 = rhs1;
    }
    return lhs;
}

template <int maxLength>
std::vector<std::string> readQuoteStrings(FILE *in) {
    std::vector<std::string> answer;
    for (;;) {
        char s[maxLength];
        if (fscanf(in, "\"%[^\"]\",", s) < 1) break;
        answer.emplace_back(s);
    }
    return answer;
}

class HashInts {
   public:
    size_t operator()(const std::vector<int> &x) const {
        return util::Hash((char *)x.data(), sizeof(x[0]) * x.size());
    }
};

#endif  // UTILITY_H
