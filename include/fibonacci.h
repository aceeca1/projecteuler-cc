#ifndef FIBONACCI_H
#define FIBONACCI_H

#include <iterator>

template <class T>
class FibonacciIt : public std::iterator<std::forward_iterator_tag, T> {
    T a1, a2;

   public:
    FibonacciIt(const T &a1_, const T &a2_) : a1(a1_), a2(a2_) {}
    const T &operator*() const { return a1; }
    FibonacciIt &operator++() {
        const T &a3 = a1 + a2;
        a1 = std::move(a2);
        a2 = std::move(a3);
        return *this;
    }
};

#endif  // FIBONACCI_H
