#ifndef DIVISOR_H
#define DIVISOR_H

#include <NTL/ZZ.h>
#include <iterator>
#include <vector>

class Divisor {
    class Iterator : public std::iterator<std::forward_iterator_tag, int> {
        int n, i, pc;

       public:
        Iterator() : pc(2) {}
        Iterator(int n_) : n(n_), i(0), pc(1) { operator++(); }
        int operator*() const { return pc ? n / i : i; }
        Iterator &operator++() {
            if (!pc) {
                pc = 1;
                return *this;
            }
            ++i;
            while (i * i < n && n % i) ++i;
            if (i * i < n) {
                pc = 0;
                return *this;
            }
            if (i * i == n) return *this;
            pc = 2;
            return *this;
        }
        bool operator!=(const Iterator &rhs) const { return pc != rhs.pc; }
    };

    int n;

   public:
    Iterator begin() const { return Iterator(n); }
    Iterator end() const { return Iterator(); }
    Divisor(int n_) : n(n_) {}
    int size() const { return std::distance(begin(), end()); }
    int sum() const {
        int answer = 0;
        for (int i : *this) answer += i;
        return answer;
    }
};

template <class T>
std::vector<T> factorize(T n) {
    std::vector<T> answer;
    NTL::PrimeSeq s;
    while (true) {
        T p = s.next();
        if (n < p * p) break;
        while (!(n % p)) {
            n /= p;
            answer.emplace_back(p);
        }
    }
    if (1 < n) answer.emplace_back(n);
    return answer;
}

#endif
