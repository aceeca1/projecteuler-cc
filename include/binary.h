#ifndef BINARY_H
#define BINARY_H

#include <algorithm>
#include <cstdint>
#include <string>

std::string toBinaryString(int n) {
    std::string answer;
    while (n) {
        answer += '0' + (n & 1);
        n >>= 1;
    }
    std::reverse(answer.begin(), answer.end());
    return answer;
}

int countLowest14Bits(std::uint32_t n) {
    return (n * 0x200040008001ULL & 0x111111111111111ULL) % 0xf;
}

int whichBit(uint32_t n) {
    static constexpr int deBruijn[32]{
        0,  1,  28, 2,  29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4,  8,
        31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6,  11, 5,  10, 9};
    return deBruijn[n * 0x077CB531U >> 27];
}

#endif  // BINARY_H
