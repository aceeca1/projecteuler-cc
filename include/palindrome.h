#ifndef PALINDROME_H
#define PALINDROME_H

#include <algorithm>
#include <string>

template <class Func>
static void foreachPalindromeBelow(int n, Func &&f) {
    for (int i = 1; i >= 0; --i) {
        for (int j = 1;; ++j) {
            const auto &s = std::to_string(j);
            std::string s1(s);
            s1.append(s.rbegin() + i, s.rend());
            int v = std::stoi(s1);
            if (n <= v) break;
            f(v);
        }
    }
}

bool isPalindrome(const std::string &s) {
    return std::equal(s.begin(), s.end(), s.rbegin());
}

bool isPalindrome(int n) { return isPalindrome(std::to_string(n)); }

#endif  // PALINDROME_H
