#ifndef DIGIT_H
#define DIGIT_H

#include <string>

int digitSum(int n, int a[]) {
    int answer = 0;
    for (char i : std::to_string(n)) answer += a[i - '0'];
    return answer;
}

template <class T>
uint64_t countDigits(T n) {
    uint64_t answer = 0;
    for (char i : std::to_string(n)) answer += 1ULL << ((i - '0') * 5);
    return answer;
}

#endif  // DIGIT_H
