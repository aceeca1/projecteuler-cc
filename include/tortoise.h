#ifndef TORTOISE_H
#define TORTOISE_H

class TortoiseHare {
   public:
    template <class T, class Next>
    static TortoiseHare create(const T &seed, Next &&next) {
        int n1 = 0, n2 = 1;
        T a1(seed), a2(seed);
        next(a2);
        while (a1 != a2) {
            if (n1 + n1 < n2) {
                n1 = n2;
                a1 = a2;
            }
            ++n2;
            next(a2);
        }
        return TortoiseHare{n2 - n1};
    }

    int cycleSize;
};

#endif  // TORTOISE_H
