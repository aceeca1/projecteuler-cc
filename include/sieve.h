#ifndef SIEVE_H
#define SIEVE_H

#include <algorithm>
#include <vector>

std::vector<int> sieve(int n) {
    std::vector<int> answer(n + 1);
    int lastPrime = 0;
    for (int i = 2; i <= n; ++i) {
        int v = answer[i];
        if (v == 0) lastPrime = answer[lastPrime] = v = i;
        for (int j = 2; i * j <= n; j = answer[j]) {
            answer[i * j] = j;
            if (j >= v) break;
        }
    }
    answer[lastPrime] = n + 1;
    return answer;
}

std::vector<int> batchPhi(int n) {
    auto answer = sieve(n);
    answer[1] = 1;
    for (int i = 2; i <= n; ++i) {
        int minFactor = std::min(answer[i], i);
        int other = i / minFactor;
        int multiplier = minFactor - (other % minFactor != 0);
        answer[i] = answer[other] * multiplier;
    }
    return answer;
}

#endif
