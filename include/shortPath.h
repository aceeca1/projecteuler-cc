#ifndef SHORTPATH_H
#define SHORTPATH_H

#include <deque>
#include <limits>
#include <vector>

template <class Graph, class CostNumeric>
class ShortPath {
   public:
    std::vector<CostNumeric> distance;
    std::vector<int> from;

    ShortPath(const Graph* graph, int source) {
        distance.resize(graph->getSizeV());
        for (int& i : distance) i = std::numeric_limits<CostNumeric>::max();
        from.resize(graph->getSizeV());
        std::deque<int> needUpdate;
        needUpdate.emplace_back(source);
        distance[source] = 0;
        while (!needUpdate.empty()) {
            int v = needUpdate.front();
            needUpdate.pop_front();
            for (auto i : graph->outEdge(v)) {
                CostNumeric newDistance = distance[v] + i.cost();
                if (distance[i.target] <= newDistance) continue;
                distance[i.target] = newDistance;
                from[i.target] = v;
                if (!needUpdate.empty() &&
                    newDistance < distance[needUpdate.front()])
                    needUpdate.emplace_front(i.target);
                else
                    needUpdate.emplace_back(i.target);
            }
        }
    }
};

#endif  // SHORTPATH_H
