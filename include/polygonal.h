#ifndef POLYGONAL_H
#define POLYGONAL_H

#include <cmath>

long long getPolygonal(int k, int n) {
    return ((long long)n * (n - 1) >> 1) * (k - 2) + n;
}

bool isPolygonal(int k, long long n) {
    if (k == 3) return n == getPolygonal(k, std::sqrt(n + n));
    if (k == 4) return n == getPolygonal(k, std::sqrt(n));
    return n == getPolygonal(k, std::sqrt((n + n) / (k - 2)) + 1.0);
}

#endif  // POLYGONAL_H
