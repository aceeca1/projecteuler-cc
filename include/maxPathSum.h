#ifndef MAX_PATH_SUM_H
#define MAX_PATH_SUM_H

#include <cstdio>
#include <vector>

int maxPathSum(int n, const char* fileName) {
    std::vector<std::vector<int>> a(n);
    auto in = std::fopen(fileName, "r");
    for (int i = 0; i < n; ++i) {
        a[i].resize(i + 1);
        for (int j = 0; j <= i; ++j) std::fscanf(in, "%d", &a[i][j]);
    }
    std::fclose(in);
    auto& answer = a[n - 1];
    for (int i = n - 2; i >= 0; --i)
        for (int j = 0; j <= i; ++j)
            answer[j] = std::max(answer[j], answer[j + 1]) + a[i][j];
    return answer[0];
}

#endif  // MAX_PATH_SUM_H
