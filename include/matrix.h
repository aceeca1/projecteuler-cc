#ifndef MATRIX_H
#define MATRIX_H

#include <vector>

template <class Numeric>
class Matrix {
   public:
    std::vector<std::vector<Numeric>> data;

    Matrix(int p1, int p2) : data(p1, std::vector<Numeric>(p2)) {}

    Matrix operator*(const Matrix& rhs) const {
        Matrix ret(data.size(), rhs.data[0].size());
        for (int i = 0; i < (int)data.size(); ++i)
            for (int j = 0; j < (int)data[0].size(); ++j)
                for (int k = 0; k < (int)rhs.data[0].size(); ++k)
                    ret.data[i][k] += data[i][j] * rhs.data[j][k];
        return ret;
    }
};

#endif  // MATRIX_H
