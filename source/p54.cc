#include <cstdio>
#include <string>
#include <vector>
using namespace std;

class Poker {
    const char* cardRank = "23456789TJQKA";
    int cardToRank[256]{};

    bool isFlush(const string hand[]) const {
        for (int i = 1; i < 5; ++i)
            if (hand[i][1] != hand[0][1]) return false;
        return true;
    }

    bool isStraight0(const int a[]) const {
        for (int i : {12, 0, 1, 2, 3})
            if (a[i] != 1) return false;
        return true;
    }

    int isStraight(const int a[]) const {
        if (isStraight0(a)) return 3;
        int k = 0;
        while (!a[k]) ++k;
        for (int i = k; i < k + 5; ++i)
            if (a[i] != 1) return 0;
        return k + 4;
    }

   public:
    Poker() {
        for (int i = 0; cardRank[i]; ++i) cardToRank[(int)cardRank[i]] = i;
    }

    string evaluate(const string hand[]) const {
        int a[13]{};
        for (int i = 0; i < 5; ++i) ++a[cardToRank[(int)hand[i][0]]];
        bool flush = isFlush(hand);
        int straight = isStraight(a);
        if (flush) return straight ? string("9") + char('A' + straight) : "6";
        if (straight) return string("5") + char('A' + straight);
        vector<int> b[5];
        for (int i = 12; i >= 0; --i)
            if (a[i]) b[a[i]].emplace_back(i);
        string answer;
        if (b[4].size())
            answer += '8';
        else if (b[3].size() && b[2].size())
            answer += '7';
        else if (b[3].size())
            answer += '4';
        else if (b[2].size() == 2)
            answer += '3';
        else if (b[2].size())
            answer += '2';
        else
            answer += '1';
        for (int i = 4; i; --i)
            for (int j : b[i]) answer += 'A' + j;
        return answer;
    }
};

int main() {
    int answer = 0;
    Poker poker;
    auto in = fopen("data/input54.txt", "r");
    for (int i = 0; i < 1000; ++i) {
        string s[10];
        for (int i = 0; i < 10; ++i) {
            char c[3];
            fscanf(in, "%2s", c);
            s[i] = c;
        }
        auto e1 = poker.evaluate(s);
        auto e2 = poker.evaluate(s + 5);
        if (e2 < e1) ++answer;
    }
    fclose(in);
    printf("%d\n", answer);
    return 0;
}
