#include <cstdio>
#include <vector>
using namespace std;

class SubstringDiv {
    vector<int> m{2, 3, 5, 7, 11, 13, 17}, a{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    void put(int k) {
        if (k == -1) {
            long long b = 1;
            for (int i = a.size() - 1; i >= 0; --i) {
                sum += b * a[i];
                b *= 10;
            }
            return;
        }
        if (k < 7) {
            int a3 = a[k + 1] * 100 + a[k + 2] * 10 + a[k + 3];
            if (a3 % m[k]) return;
        }
        for (int i = 0; i <= k; ++i) {
            swap(a[i], a[k]);
            put(k - 1);
            swap(a[i], a[k]);
        }
    }

   public:
    long long sum = 0;
    SubstringDiv() { put(9); }
};

int main() { printf("%lld\n", SubstringDiv().sum); }
