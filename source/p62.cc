#include <cstdio>
#include <string>
#include <unordered_map>
#include <vector>
#include "../include/digit.h"
using namespace std;

class CubicPermutations {
    int numLength = 0;
    unordered_map<uint64_t, vector<long long>> hashCube;

    bool foundReplicate() {
        for (const auto& i : hashCube)
            if (5 <= i.second.size()) {
                answer = i.second[0];
                return true;
            }
        return false;
    }

   public:
    long long answer;

    void solve() {
        for (int i = 1;; ++i) {
            auto cube = (long long)i * i * i;
            auto s = to_string(cube);
            if ((int)s.size() != numLength) {
                if (foundReplicate()) return;
                hashCube.clear();
                numLength = s.size();
            }
            hashCube[countDigits(cube)].emplace_back(cube);
        }
    }
};

int main() {
    CubicPermutations c;
    c.solve();
    printf("%lld\n", c.answer);
    return 0;
}
