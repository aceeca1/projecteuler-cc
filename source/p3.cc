#include <algorithm>
#include <cstdio>
#include "../include/divisor.h"
using namespace std;

constexpr auto N = 600851475143LL;

int main() {
    auto answer = factorize(N);
    printf("%lld\n", *max_element(answer.begin(), answer.end()));
    return 0;
}
