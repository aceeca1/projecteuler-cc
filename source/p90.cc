#include <cstdio>
#include <vector>
using namespace std;

class Cube {
   public:
    vector<int> collection;

    int derive69(int n) {
        if ((n >> 6) & 1) n |= 1 << 9;
        if ((n >> 9) & 1) n |= 1 << 6;
        return n;
    }

    void search(int size, int current, int bits) {
        if (size == 6) {
            collection.emplace_back(derive69(bits));
            return;
        }
        if (10 <= current) return;
        search(size + 1, current + 1, bits + (1 << current));
        search(size, current + 1, bits);
    }

    Cube() { search(0, 0, 0); }
};

bool canMakeAllSquares(int bits1, int bits2) {
    for (int i = 1; i <= 9; ++i) {
        int iSquare = i * i, a1 = iSquare / 10, a2 = iSquare % 10;
        if (!(((bits1 >> a1) & 1) && ((bits2 >> a2) & 1)) &&
            !(((bits1 >> a2) & 1) && ((bits2 >> a1) & 1)))
            return false;
    }
    return true;
}

int main() {
    Cube c;
    int answer = 0;
    for (int i1 : c.collection)
        for (int i2 : c.collection)
            if (canMakeAllSquares(i1, i2)) ++answer;
    printf("%d\n", answer >> 1);
    return 0;
}
