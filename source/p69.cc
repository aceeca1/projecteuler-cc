#include <cstdio>
#include "../include/sieve.h"
#include "../include/utility.h"
using namespace std;

constexpr int N = 1000000;

int main() {
    double maxRatio = 0.0;
    int arg = -1;
    auto phi = batchPhi(N);
    for (int i = 2; i <= N; ++i)
        assignMax(maxRatio, (double)i / phi[i], arg, i);
    printf("%d\n", arg);
    return 0;
}
