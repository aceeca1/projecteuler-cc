#include <NTL/ZZ.h>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

int suchPrime() {
    NTL::PrimeSeq s;
    while (true) {
        int pp = s.next();
        auto p = to_string(pp);
        for (char c : "012") {
            if (p.find(c) == string::npos) continue;
            vector<string> v{""};
            for (char pi : p)
                for (int i = v.size() - 1; i >= 0; --i) {
                    if (pi == c) v.emplace_back(v[i] + '*');
                    v[i] += pi;
                }
            v[0] = v.back();
            v.pop_back();
            for (string& vi : v) {
                int num = 0;
                for (char cc = c + 1; cc <= '9'; ++cc) {
                    string vic(vi);
                    for (char& i : vic)
                        if (i == '*') i = cc;
                    if (NTL::ProbPrime(stoi(vic))) ++num;
                }
                if (num >= 7) return pp;
            }
        }
    }
}

int main() {
    printf("%d\n", suchPrime());
    return 0;
}
