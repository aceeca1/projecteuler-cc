#include <cstdio>
#include <string>
#include <tuple>
#include <vector>
using namespace std;

class RomanNumerals {
    int toNum[256]{};

    tuple<int, string> fromNum[13]{
        {1000, "M"}, {900, "CM"}, {500, "D"}, {400, "CD"},  //
        {100, "C"},  {90, "XC"},  {50, "L"},  {40, "XL"},   //
        {10, "X"},   {9, "IX"},   {5, "V"},   {4, "IV"},   {1, "I"},
    };

   public:
    RomanNumerals() {
        for (const auto& i : fromNum)
            if (get<1>(i).size() == 1) toNum[(int)get<1>(i)[0]] = get<0>(i);
    }

    int toInt(const string& s) {
        int answer = 0, previous = 0;
        for (char i : s) {
            int current = toNum[(int)i];
            if (previous == 0) {
                previous = current;
            } else if (previous < current) {
                answer += current - previous;
                previous = 0;
            } else {
                answer += previous;
                previous = current;
            }
        }
        return answer + previous;
    }

    string fromInt(int n) {
        string answer;
        for (const auto& i : fromNum) {
            while (get<0>(i) <= n) {
                n -= get<0>(i);
                answer += get<1>(i);
            }
        }
        return answer;
    }
};

int main() {
    RomanNumerals roman;
    int answer = 0;
    auto in = fopen("../data/input89.txt", "r");
    while (true) {
        char s[80];
        if (fscanf(in, "%s", s) < 1) break;
        string ss(s);
        answer += ss.size() - roman.fromInt(roman.toInt(ss)).size();
    }
    fclose(in);
    printf("%d\n", answer);
    return 0;
}
