#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

int main() {
    NTL::ZZ answer(NTL::INIT_VAL, 1);
    for (int i = 2; i <= 100; ++i) answer *= i;
    stringstream strs;
    strs << answer;
    int answer1 = 0;
    for (char i : strs.str()) answer1 += i - '0';
    printf("%d\n", answer1);
    return 0;
}
