#include <cstdio>
using namespace std;

constexpr auto N = 1000;
constexpr auto M = 10000000000;

int main() {
    long long answer = 0;
    for (int i = 1; i <= N; ++i) {
        long long a = i;
        for (int j = 2; j <= i; ++j) a = a * i % M;
        answer += a;
    }
    printf("%lld\n", answer % M);
    return 0;
}
