#include <cstdio>
#include "../include/digit.h"
using namespace std;

int main() {
    int factorial[10]{1}, answer = 0;
    for (int i = 1; i < 10; ++i) factorial[i] = factorial[i - 1] * i;
    for (int i = factorial[9] * 7; i >= 10; --i)
        if (digitSum(i, factorial) == i) answer += i;
    printf("%d\n", answer);
    return 0;
}
