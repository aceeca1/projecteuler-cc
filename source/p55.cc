#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

bool isLychrel(int n) {
    NTL::ZZ z(NTL::INIT_VAL, n);
    for (int i = 0; i < 50; ++i) {
        stringstream ss;
        ss << z;
        auto s = ss.str();
        string sR(s.rbegin(), s.rend());
        if (i && s == sR) return false;
        NTL::ZZ zR(NTL::INIT_VAL, sR.c_str());
        z += zR;
    }
    return true;
}

int main() {
    int answer = 0;
    for (int i = 1; i < 10000; ++i)
        if (isLychrel(i)) ++answer;
    printf("%d\n", answer);
    return 0;
}
