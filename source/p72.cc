#include <cstdio>
#include "../include/sieve.h"
using namespace std;

constexpr int N = 1000000;

int main() {
    auto phi = batchPhi(N);
    long long sum = 0;
    for (int i = 2; i <= N; ++i) sum += phi[i];
    printf("%lld\n", sum);
    return 0;
}
