#include <NTL/ZZ.h>
#include <cstdio>
#include <string>
#include "../include/utility.h"
using namespace std;

constexpr auto N = 10000000;

bool hasOneToM(const string &s) {
    int ss = 0;
    for (char i : s) ss |= 1 << (i - '0');
    return ss == ((1 << s.size()) - 1) << 1;
}

int main() {
    NTL::PrimeSeq s;
    int answer = 0;
    while (true) {
        int p = s.next();
        if (N <= p) break;
        if (hasOneToM(to_string(p))) assignMax(answer, p);
    }
    printf("%d\n", answer);
    return 0;
}
