#include <cstdio>
#include "../include/utility.h"
using namespace std;

int main() {
    int a[3][256]{}, c = 0;
    auto in = fopen("../data/input59.txt", "r");
    while (true) {
        int k;
        if (fscanf(in, "%d,", &k) < 1) break;
        ++a[c][k];
        if (++c == 3) c = 0;
    }
    fclose(in);
    int sum = 0;
    for (int i = 0; i < 3; ++i) {
        int maxOccurrence = 0, arg = -1;
        for (int j = 0; j < 256; ++j) assignMax(maxOccurrence, a[i][j], arg, j);
        char key = arg ^ ' ';
        for (int j = 0; j < 256; ++j) sum += a[i][j] * (j ^ key);
    }
    printf("%d\n", sum);
    return 0;
}
