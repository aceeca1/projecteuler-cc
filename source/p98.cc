#include <farmhash.h>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <string>
#include <unordered_map>
#include <vector>
#include "../include/utility.h"
using namespace std;

constexpr int POWER10[10]{1,      10,      100,      1000,      10000,
                          100000, 1000000, 10000000, 100000000, 1000000000};

uint64_t unorderedHash(const string& s) {
    uint64_t answer = 0;
    for (char i : s) answer += util::Fingerprint(i);
    return answer;
}

bool isOneToOne(const string& s1, const string& s2, int mapTable[]) {
    for (int i = 0; i < (int)s1.size(); ++i) {
        mapTable[(int)s1[i]] = s2[i];
        mapTable[(int)s2[i]] = s1[i];
    }
    for (int i = 0; i < (int)s1.size(); ++i) {
        if (mapTable[(int)s1[i]] != s2[i]) return false;
        if (mapTable[(int)s2[i]] != s1[i]) return false;
    }
    return true;
}

bool isSquare(int n, int* sqrtN) {
    *sqrtN = sqrt(n);
    return n == *sqrtN * *sqrtN;
}

void updateAnswer(int* answer, const string& s1, const string& s2) {
    int n = s1.size();
    int lower = lround(sqrt(POWER10[n - 1]));
    int upper = lround(sqrt(POWER10[n] - 1));
    for (int i1 = lower; i1 <= upper; ++i1) {
        auto i1Square = to_string(i1 * i1);
        if ((int)i1Square.size() != n) continue;
        int mapTable[256], i2;
        if (!isOneToOne(s1, i1Square, mapTable)) continue;
        auto i2Square = s2;
        for (char& i : i2Square) i = mapTable[(int)i];
        if (i2Square[0] == '0' || !isSquare(stoi(i2Square), &i2)) continue;
        assignMax(*answer, max(i1, i2) * max(i1, i2));
    }
}

int main() {
    auto in = fopen("../data/input42.txt", "r");
    const auto& a = readQuoteStrings<80>(in);
    fclose(in);
    unordered_map<uint64_t, vector<string>> u;
    for (const auto& i : a) u[unorderedHash(i)].emplace_back(i);
    int answer = 0;
    for (const auto& ui : u) {
        const auto& anagrams = ui.second;
        for (int i1 = 0; i1 < (int)anagrams.size(); ++i1)
            for (int i2 = i1 + 1; i2 < (int)anagrams.size(); ++i2)
                updateAnswer(&answer, anagrams[i1], anagrams[i2]);
    }
    printf("%d\n", answer);
}
