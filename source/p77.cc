#include <NTL/ZZ.h>
#include <cstdio>
#include <vector>
using namespace std;

constexpr int N = 5000;

vector<int> waysAsPrimeSum(int n) {
    vector<int> answer(n + 1);
    answer[0] = 1;
    NTL::PrimeSeq s;
    while (true) {
        int p = s.next();
        if (n < p) break;
        for (int i = p; i <= n; ++i) answer[i] += answer[i - p];
    }
    return answer;
}

bool hasOverN(const vector<int>& v, int* index) {
    for (*index = 0; *index < (int)v.size(); ++*index)
        if (N < v[*index]) return true;
    return false;
}

int main() {
    int n = 1, answer;
    while (!hasOverN(waysAsPrimeSum(n), &answer)) n += n;
    printf("%d\n", answer);
    return 0;
}
