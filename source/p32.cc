#include <cstdio>
#include <string>
#include <unordered_set>
#include <vector>
using namespace std;

bool hasOneToNine(const vector<int> &a) {
    int s = 0;
    for (int i : a)
        for (char j : to_string(i)) s |= 1 << (j - '0');
    return s == 0x3fe;
}

template <class Func>
void f(int n1, int n2, int n3, Func &&f) {
    for (int i1 = n1 / 10; i1 < n1; ++i1)
        for (int i2 = n2 / 10; i2 < n2; ++i2) {
            int i3 = i1 * i2;
            if (n3 < i3) break;
            if (hasOneToNine({i1, i2, i3})) f(i3);
        }
}

int main() {
    unordered_set<int> u;
    f(100, 1000, 10000, [&u](int n) { u.emplace(n); });
    f(10, 10000, 10000, [&u](int n) { u.emplace(n); });
    int answer = 0;
    for (auto i : u) answer += i;
    printf("%d\n", answer);
    return 0;
}
