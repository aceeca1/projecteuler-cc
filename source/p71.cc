#include <cstdio>
#include "../include/utility.h"
using namespace std;

int main() {
    double maxRatio = 0.0;
    int arg = -1;
    for (int i = 1; i <= 1000000; ++i) {
        int num = (i * 3 - 1) / 7;
        assignMax(maxRatio, (double)num / i, arg, num);
    }
    printf("%d\n", arg);
    return 0;
}
