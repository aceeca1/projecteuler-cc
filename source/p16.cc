#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

int main() {
    stringstream strs;
    strs << (NTL::ZZ(NTL::INIT_VAL, 1) << 1000);
    int answer = 0;
    for (char i : strs.str()) answer += i - '0';
    printf("%d\n", answer);
    return 0;
}
