#include <cstdio>
#include "../include/maxPathSum.h"
using namespace std;

int main() {
    printf("%d\n", maxPathSum(15, "../data/input18.txt"));
    return 0;
}
