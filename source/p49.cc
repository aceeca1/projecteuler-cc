#include <NTL/ZZ.h>
#include <cstdint>
#include <cstdio>
#include <unordered_map>
#include <vector>
#include "../include/digit.h"
using namespace std;

long long solve() {
    unordered_map<uint64_t, vector<int>> u;
    NTL::PrimeSeq s;
    while (true) {
        int p = s.next();
        if (p < 1000) continue;
        if (10000 <= p) break;
        u[countDigits(p)].emplace_back(p);
    }
    for (const auto &i : u) {
        const auto &v = i.second;
        for (int i1 = 0; i1 < (int)v.size(); ++i1)
            for (int i2 = i1 + 1; i2 < (int)v.size(); ++i2)
                for (int i3 = i2 + 1; i3 < (int)v.size(); ++i3)
                    if (v[i2] - v[i1] == v[i3] - v[i2] && v[i1] != 1487) {
                        long long answer = v[i1];
                        answer = answer * 10000 + v[i2];
                        answer = answer * 10000 + v[i3];
                        return answer;
                    }
    }
    return 0;
}

int main() {
    printf("%lld\n", solve());
    return 0;
}
