#include <cstdio>
#include "../include/tortoise.h"
#include "../include/utility.h"
using namespace std;

constexpr auto N = 1000;

int main() {
    int maxLength = 0, arg = -1;
    for (int i = 1; i < N; ++i) {
        auto t = TortoiseHare::create(1, [i](int &n) { n = n * 10 % i; });
        assignMax(maxLength, t.cycleSize, arg, i);
    }
    printf("%d\n", arg);
    return 0;
}
