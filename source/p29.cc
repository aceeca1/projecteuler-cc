#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
#include <unordered_set>
using namespace std;

constexpr auto N = 100;

int main() {
    unordered_set<string> u;
    for (int i = 2; i <= N; ++i) {
        NTL::ZZ a(NTL::INIT_VAL, i);
        for (int j = 2; j <= N; ++j) {
            stringstream strs;
            strs << (a *= i);
            u.emplace(strs.str());
        }
    }
    printf("%d\n", (int)u.size());
}
