#include <cstdio>
#include <string>
#include "../include/utility.h"
using namespace std;

constexpr auto N = 20, N1 = 50, M = 13;

class ProductWithZero {
    long long product = 1;
    int zero = 0;

   public:
    void add(char c) {
        if (c == '0')
            ++zero;
        else
            product *= c - '0';
    }

    void del(char c) {
        if (c == '0')
            --zero;
        else
            product /= c - '0';
    }

    long long getProduct() const { return zero ? 0 : product; }
};

int main() {
    auto in = fopen("../data/input8.txt", "r");
    string str;
    for (int i = 0; i < N; ++i) {
        char s[N1 + 2];
        fgets(s, sizeof(s), in);
        s[N1] = 0;
        str += s;
    }
    fclose(in);
    ProductWithZero pwz;
    long long answer = 0;
    for (int i = 0; i < M; ++i) pwz.add(str[i]);
    for (int i = M;; ++i) {
        auto product = pwz.getProduct();
        assignMax(answer, product);
        if (i == (int)str.size()) break;
        pwz.del(str[i - 13]);
        pwz.add(str[i]);
    }
    printf("%lld\n", answer);
    return 0;
}
