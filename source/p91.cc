#include <NTL/ZZ.h>
#include <algorithm>
#include <cstdio>
using namespace std;

/* Enumerate one point and count different positions of the other. */

int main() {
    int answer = 0;
    for (int i1 = 1; i1 <= 50; ++i1)
        for (int i2 = 1; i2 <= 50; ++i2)
            answer += min(50 - i1, i2 * i2 / i1) / (i2 / NTL::GCD(i1, i2));
    printf("%d\n", (answer << 1) + 3 * 50 * 50);
}
