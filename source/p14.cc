#include <cstdio>
#include <vector>
#include "../include/utility.h"
using namespace std;

constexpr auto N = 1000000;

class Collatz {
    vector<int> a;

    static long long next(long long n) {
        return n & 1 ? n + n + n + 1 : n >> 1;
    }

    int length(long long n) {
        if (n > N) return length(next(n)) + 1;
        if (a[n]) return a[n];
        return a[n] = length(next(n)) + 1;
    }

   public:
    int maxLength = 0, arg = -1;

    Collatz() : a(N + 1) {
        a[1] = 1;
        for (int i = 1; i <= N; ++i) assignMax(maxLength, length(i), arg, i);
    }
};

int main() {
    printf("%d\n", Collatz().arg);
    return 0;
}
