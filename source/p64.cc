#include <cmath>
#include <cstdio>
#include <vector>
#include "../include/tortoise.h"
using namespace std;

bool oddPeriod(int n) {
    int sqrtN = sqrt(n);
    if (sqrtN * sqrtN == n) return false;
    vector<int> a0{0, 0, 1};
    auto t = TortoiseHare::create(a0, [&](auto& ai) {
        ai[2] = (n - ai[1] * ai[1]) / ai[2];
        ai[0] = (sqrtN - ai[1]) / ai[2];
        ai[1] = -ai[0] * ai[2] - ai[1];
    });
    return t.cycleSize & 1;
}

int main() {
    int answer = 0;
    for (int i = 1; i <= 10000; ++i)
        if (oddPeriod(i)) ++answer;
    printf("%d\n", answer);
    return 0;
}
