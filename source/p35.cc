#include <NTL/ZZ.h>
#include <algorithm>
#include <cstdio>
#include <unordered_set>
using namespace std;

constexpr auto N = 1000000;

int main() {
    unordered_set<string> primeSet;
    NTL::PrimeSeq s;
    while (true) {
        int p = s.next();
        if (N <= p) break;
        primeSet.emplace(to_string(p));
    }
    for (int i : {4, 2, 1}) {
        unordered_set<string> newSet;
        for (auto j : primeSet) {
            rotate(j.begin(), j.begin() + i % j.size(), j.end());
            if (primeSet.count(j)) newSet.emplace(j);
        }
        primeSet = move(newSet);
    }
    printf("%d\n", (int)primeSet.size());
    return 0;
}
