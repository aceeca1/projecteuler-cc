#include <cstdio>
using namespace std;

int solve() {
    for (int a = 1;; ++a) {
        int s1 = 1000 - a;
        int s2 = a * a;
        if (s2 % s1) continue;
        int b2 = s1 - s2 / s1;
        if (b2 < 0 || b2 & 1) continue;
        int b = b2 >> 1;
        int c = s1 - b;
        return a * b * c;
    }
}

int main() {
    printf("%d\n", solve());
    return 0;
}
