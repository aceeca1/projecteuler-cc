#include <NTL/ZZ.h>
#include <cstdio>
#include "../include/utility.h"
using namespace std;

constexpr auto N = 1000000;

int main() {
    int maxLength = 1, arg = -1;
    NTL::PrimeSeq s;
    while (true) {
        int p = s.next();
        if (N <= p * maxLength) break;
        NTL::PrimeSeq s1;
        s1.reset(p + 1);
        for (int i = 2;; ++i) {
            p += s1.next();
            if (N <= p) break;
            if (NTL::ProbPrime(p)) assignMax(maxLength, i, arg, p);
        }
    }
    printf("%d\n", arg);
    return 0;
}
