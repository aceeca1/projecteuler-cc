#include <cstdio>
#include <initializer_list>
using namespace std;

constexpr auto N = 200;

int main() {
    int a[N + 1]{1};
    for (int i : {1, 2, 5, 10, 20, 50, 100, 200})
        for (int j = i; j <= N; ++j) a[j] += a[j - i];
    printf("%d\n", a[N]);
    return 0;
}
