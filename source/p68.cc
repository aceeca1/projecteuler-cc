#include <cstdio>
#include <initializer_list>
#include <string>
#include <vector>
using namespace std;

class MagicRing {
    int num[10], sum;

    vector<int> getBounds(int k) {
        if (k == 0) return {6, 1};
        if ((0x2a8 >> k) & 1) return {9, num[0]};  // k = 3, 5, 7, 9
        return {9, 1};
    }

    bool isReplicate(int k) {
        for (int i = 0; i < k; ++i)
            if (num[i] == num[k]) return true;
        return false;
    }

    bool isSameSum(int k) {
        switch (k) {
            case 4:
            case 6:
            case 8:
                return sum == num[k] + num[k - 1] + num[k - 2];
            case 9:
                return sum == num[1] + num[8] + num[9];
            case 2:
                sum = num[0] + num[1] + num[2];
        }
        return true;
    }

   public:
    bool search(int k) {
        if (k >= 10) return true;
        auto bounds = getBounds(k);
        for (int i = bounds[0]; i >= bounds[1]; --i) {
            num[k] = (0x2a8 >> k) & 1 && i == bounds[1] ? 10 : i;
            if (isReplicate(k) || !isSameSum(k)) continue;
            if (search(k + 1)) return true;
        }
        return false;
    }

    string concatenation() {
        string answer;
        for (int i : {0, 1, 2, 3, 2, 4, 5, 4, 6, 7, 6, 8, 9, 8, 1})
            answer += to_string(num[i]);
        return answer;
    }
};

int main() {
    MagicRing mr;
    mr.search(0);
    printf("%s\n", mr.concatenation().c_str());
    return 0;
}
