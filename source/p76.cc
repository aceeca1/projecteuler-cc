#include <cstdio>
#include <vector>
using namespace std;

constexpr int N = 100;

int main() {
    vector<int> ways(N);
    ways[0] = 1;
    for (int i = 1; i <= N; ++i)
        for (int j = i; j <= N; ++j) ways[j] += ways[j - i];
    printf("%d\n", ways[N] - 1);
    return 0;
}
