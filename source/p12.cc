#include <cstdio>
#include "../include/divisor.h"
using namespace std;

int main() {
    int n = 1, step = 2;
    while (Divisor(n).size() <= 500) n += step++;
    printf("%d\n", n);
    return 0;
}
