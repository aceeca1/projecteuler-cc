#include <NTL/ZZ.h>
#include <climits>
#include <cstdio>
#include <vector>
using namespace std;

class PPSet {
    vector<string> content;
    vector<int> contentOrigin;

   public:
    int sum = 0;

    int size() const { return content.size(); }
    int back() const { return size() ? contentOrigin.back() : 0; }

    bool tryPush(int n) {
        auto s = to_string(n);
        for (auto i : content) {
            if (!NTL::ProbPrime(NTL::ZZ(NTL::INIT_VAL, (i + s).c_str())))
                return false;
            if (!NTL::ProbPrime(NTL::ZZ(NTL::INIT_VAL, (s + i).c_str())))
                return false;
        }
        content.emplace_back(move(s));
        contentOrigin.emplace_back(n);
        sum += n;
        return true;
    }

    void pop() {
        content.pop_back();
        sum -= contentOrigin.back();
        contentOrigin.pop_back();
    }
};

class PPSetSearcher {
    PPSet current;

   public:
    int target, minSum;

    PPSetSearcher(int target_, int minSum_)
        : target(target_), minSum(minSum_) {}

    void search() {
        if (current.size() == target) {
            if (current.sum < minSum) minSum = current.sum;
            return;
        }
        NTL::PrimeSeq s;
        s.reset(current.back() + 1);
        while (true) {
            int p = s.next();
            if (minSum <= (target - current.size()) * p + current.sum) break;
            if (current.tryPush(p)) {
                search();
                current.pop();
            }
        }
    }
};

int solve() {
    int limit = 1;
    while (true) {
        PPSetSearcher searcher(5, limit);
        searcher.search();
        if (searcher.minSum != limit) return searcher.minSum;
        limit += limit;
    }
}

int main() {
    printf("%d\n", solve());
    return 0;
}
