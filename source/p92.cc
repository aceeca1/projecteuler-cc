#include <cstdio>
#include <vector>
#include "../include/digit.h"
using namespace std;

constexpr int N = 10000000;

class SquareDigitChains {
    int square[10];

   public:
    vector<int> endTo;

    SquareDigitChains() : endTo(N) {
        for (int i = 0; i < 10; ++i) square[i] = i * i;
        endTo[1] = 1;
        endTo[89] = 89;
        for (int i = 1; i < N; ++i)
            if (endTo[i] == 0) startFrom(i);
    }

    void startFrom(int k) {
        vector<int> visited;
        while (endTo[k] == 0) {
            visited.emplace_back(k);
            k = digitSum(k, square);
        }
        for (int i : visited) endTo[i] = endTo[k];
    }
};

int main() {
    SquareDigitChains chains;
    int answer = 0;
    for (int i = 1; i < N; ++i)
        if (chains.endTo[i] == 89) ++answer;
    printf("%d\n", answer);
    return 0;
}
