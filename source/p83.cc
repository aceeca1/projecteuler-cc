#include <cstdio>
#include <tuple>
#include <vector>
#include "../include/graph.h"
#include "../include/shortPath.h"
#include "../include/utility.h"
using namespace std;

constexpr int N = 80;

int main() {
    vector<vector<int>> a(N, vector<int>(N));
    auto in = fopen("../data/input81.txt", "r");
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; ++j) fscanf(in, "%d,", &a[i][j]);
    fclose(in);
    EdgeListGraph<CostEdge> graph;
    GraphBuilder<vector<int>, CostEdge, HashInts> builder(&graph);
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            int v = builder.addVertex({i, j});
            if (j) {
                int v1 = builder.addVertex({i, j - 1});
                graph.out[v1].emplace_back(CostEdge{v, a[i][j]});
                graph.out[v].emplace_back(CostEdge{v1, a[i][j - 1]});
            }
            if (i) {
                int v1 = builder.addVertex({i - 1, j});
                graph.out[v1].emplace_back(CostEdge{v, a[i][j]});
                graph.out[v].emplace_back(CostEdge{v1, a[i - 1][j]});
            }
        }
    }
    int source = builder.addVertex({0, 0});
    int target = builder.addVertex({N - 1, N - 1});
    ShortPath<EdgeListGraph<CostEdge>, int> shortPath(&graph, source);
    printf("%d\n", shortPath.distance[target] + a[0][0]);
    return 0;
}
