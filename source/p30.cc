#include <cstdio>
#include "../include/digit.h"
using namespace std;

int main() {
    int power5[10], answer = 0;
    for (int i = 0; i < 10; ++i) power5[i] = i * i * i * i * i;
    for (int i = power5[9] * 6; i >= 10; --i)
        if (digitSum(i, power5) == i) answer += i;
    printf("%d\n", answer);
    return 0;
}
