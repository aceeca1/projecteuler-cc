#include <cstdio>
#include "../include/palindrome.h"
#include "../include/utility.h"
using namespace std;

int main() {
    int answer = 0;
    for (int i1 = 100; i1 < 1000; ++i1)
        for (int i2 = 100; i2 < 1000; ++i2) {
            int n = i1 * i2;
            if (isPalindrome(n)) assignMax(answer, n);
        }
    printf("%d\n", answer);
    return 0;
}
