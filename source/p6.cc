#include <cstdio>
using namespace std;

constexpr auto N = 100;

int main() {
    int answer1 = 0, answer2 = 0;
    for (int i = 1; i <= N; ++i) {
        answer1 += i;
        answer2 += i * i;
    }
    printf("%d\n", answer1 * answer1 - answer2);
    return 0;
}
