#include <cstdio>
#include <vector>
#include "../include/divisor.h"
using namespace std;

constexpr auto N = 28124;

class Abundant {
   public:
    vector<int> all;
    vector<bool> is;

    Abundant(int n) : is(n + 1) {
        for (int i = 2; i < n; ++i) {
            if (Divisor(i).sum() - i <= i) continue;
            all.emplace_back(i);
            is[i] = true;
        }
    }

    bool isSum(int n) const {
        for (int i : all)
            if (n - i >= 0 && is[n - i]) return true;
        return false;
    }
};

int main() {
    Abundant ab(N);
    int answer = 0;
    for (int i = 1; i < N; ++i)
        if (!ab.isSum(i)) answer += i;
    printf("%d\n", answer);
    return 0;
}
