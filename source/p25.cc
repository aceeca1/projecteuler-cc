#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
#include "../include/fibonacci.h"
using namespace std;

int main() {
    int answer = 1;
    NTL::ZZ one(NTL::INIT_VAL, 1);
    for (FibonacciIt<NTL::ZZ> fib(one, one);; ++fib) {
        stringstream strs;
        strs << *fib;
        if (1000 <= strs.str().size()) break;
        ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
