#include <cstdio>
#include <vector>
using namespace std;

class Pronounce {
    vector<int> x{0, 3, 3, 5, 4, 4, 3, 5, 5, 4, 3,
                  6, 6, 8, 8, 7, 7, 9, 8, 8, 6},
        x0{0, 3, 6, 6, 5, 5, 5, 7, 6, 6};

   public:
    int length2(int n) const {
        if (n <= 20) return x[n];
        int n0 = n % 10, n1 = n / 10;
        return x0[n1] + x[n0];
    }

    int length(int n) const {
        if (n < 100) return length2(n);
        if (n == 1000) return 11;
        int n0 = n % 100, n2 = n / 100;
        int answer = x[n2] + 7;
        if (n0) answer += 3 + length2(n0);
        return answer;
    }
};

int main() {
    Pronounce pronounce;
    int answer = 0;
    for (int i = 1; i <= 1000; ++i) {
        answer += pronounce.length(i);
    }
    printf("%d\n", answer);
    return 0;
}
