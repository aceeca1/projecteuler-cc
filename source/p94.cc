#include <cmath>
#include <cstdio>
using namespace std;

/* Let p be half of the unique edge, and we have
    (2p +- 1)^2 - p^2 = (s / p)^2 .
Then (s / p) must be integer. So,
    (2p +- 1)^2 - p^2 = h^2 .
Rewrite it to get pell equations,
    (3p +- 2)^2 - 3h^2 = 1 .
Use wolframalpha.com to get the solutions. */

constexpr auto N = 1000000000L;
constexpr double SQRT3 = sqrt(3.0);
constexpr double B = 7.0 + 4.0 * SQRT3;

long long sum1() {
    long long answer = 0;
    double a = (2.0 - SQRT3) * B;
    while (true) {
        a *= B;
        auto v = llround(a) - 2;
        if (N < v) return answer;
        answer += v;
    }
}

long long sum2() {
    long long answer = 0;
    double a = 1.0;
    while (true) {
        a *= B;
        auto v = llround(a) + 2;
        if (N < v) return answer;
        answer += v;
    }
}

int main() {
    printf("%lld\n", sum1() + sum2());
    return 0;
}
