#include <cmath>
#include <cstdio>
using namespace std;

constexpr double SQRT2 = sqrt(2.0);
constexpr double C = 3.0 + 2.0 * SQRT2;
constexpr auto N = 1000000000000LL;

long long solve() {
    double a = (2.0 - SQRT2) * 0.125;
    double b = (SQRT2 - 1.0) * 0.25;
    while (true) {
        a *= C;
        b *= C;
        if (N < llround(b + 0.5)) return llround(a + 0.5);
    }
}

int main() {
    printf("%lld\n", solve());
    return 0;
}
