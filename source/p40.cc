#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    unordered_set<int> u{1, 10, 100, 1000, 10000, 100000, 1000000};
    int answer = 1;
    for (int i = 1, c = 1; c <= 1000000; ++i)
        for (char j : to_string(i))
            if (u.count(c++)) answer *= (j - '0');
    printf("%d\n", answer);
    return 0;
}
