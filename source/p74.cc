#include <cstdio>
#include <vector>
#include "../include/digit.h"
using namespace std;

constexpr int N = 1000000;

class FactorialChains {
    int factorial[10]{1};

   public:
    vector<int> cycle;

    FactorialChains() {
        for (int i = 1; i < 10; ++i) factorial[i] = factorial[i - 1] * i;
        cycle.resize(factorial[9] * 7 + 1);
        for (int i = 1; i < N; ++i)
            if (cycle[i] == 0) startFrom(i);
    }

    void startFrom(int k) {
        vector<int> visited;
        while (cycle[k] == 0) {
            visited.emplace_back(k);
            cycle[k] = -visited.size();
            k = digitSum(k, factorial);
        }
        int firstRepeated = cycle[k] > 0 ? visited.size() : ~cycle[k];
        int cycleLength = cycle[k] > 0 ? cycle[k] : visited.size() - ~cycle[k];
        for (int i = visited.size() - 1; i >= firstRepeated; --i)
            cycle[visited[i]] = cycleLength;
        for (int i = firstRepeated - 1; i >= 0; --i)
            cycle[visited[i]] = ++cycleLength;
    }
};

int main() {
    FactorialChains chains;
    int answer = 0;
    for (int i = 1; i < N; ++i)
        if (chains.cycle[i] == 60) ++answer;
    printf("%d\n", answer);
    return 0;
}
