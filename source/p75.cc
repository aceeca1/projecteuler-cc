#include <NTL/ZZ.h>
#include <cstdio>
#include <vector>
using namespace std;

/* All Pythagorean triples can be generated from
   (k(p^2 - q^2))^2 + (k(2pq)^2)^2 = (k(p^2 + q^2))^2 .
   The sum is 2p(p + q) = u. */

constexpr int N = 1500000;

int main() {
    vector<int> ways(N + 1);
    for (int p = 1; p * (p + 1) << 1 <= N; ++p)
        for (int q = p - 1; q >= 1; q -= 2) {
            int u = p * (p + q) << 1;
            if (u <= N && NTL::GCD(p, q) == 1)
                for (int i = u; i <= N; i += u) ++ways[i];
        }
    int answer = 0;
    for (int i = 0; i <= N; ++i)
        if (ways[i] == 1) ++answer;
    printf("%d\n", answer);
    return 0;
}
