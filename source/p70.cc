#include <cmath>
#include <cstdio>
#include "../include/digit.h"
#include "../include/sieve.h"
#include "../include/utility.h"
using namespace std;

const int N = 10000000;

int main() {
    double minRatio = INFINITY;
    int arg = -1;
    auto phi = batchPhi(N);
    for (int i = 2; i < N; ++i)
        if (countDigits(i) == countDigits(phi[i]))
            assignMin(minRatio, (double)i / phi[i], arg, i);
    printf("%d\n", arg);
    return 0;
}
