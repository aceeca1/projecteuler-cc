#include <climits>
#include <cstdio>
#include <vector>
#include "../include/divisor.h"
#include "../include/utility.h"
using namespace std;

constexpr int N = 1000000;

class AmicableChains {
   public:
    vector<int> cycle;

    AmicableChains() : cycle(N + 1) {
        for (int i = 1; i <= N; ++i)
            if (cycle[i] == 0) startFrom(i);
    }

    void startFrom(int k) {
        vector<int> visited;
        while (cycle[k] == 0) {
            visited.emplace_back(k);
            cycle[k] = -visited.size();
            k = Divisor(k).sum() - k;
            if (N < k) break;
        }
        if (N < k || 0 < cycle[k])
            for (int i : visited) cycle[i] = INT_MAX;
        else {
            int firstRepeated = ~cycle[k];
            int cycleLength = visited.size() - ~cycle[k];
            for (int i = visited.size() - 1; i >= firstRepeated; --i)
                cycle[visited[i]] = cycleLength;
            for (int i = firstRepeated - 1; i >= 0; --i)
                cycle[visited[i]] = INT_MAX;
        }
    }
};

int main() {
    AmicableChains chains;
    int maxLength = 0, arg = -1;
    for (int i = 1; i <= N; ++i)
        if (chains.cycle[i] != INT_MAX)
            assignMax(maxLength, chains.cycle[i], arg, i);
    printf("%d\n", arg);
    return 0;
}
