#include <cstdio>
using namespace std;

constexpr auto N = 500;

int main() {
    int a1 = N * (N + 1) * (N + N + 1) / 6;
    int a2 = N * (N + 1) >> 1;
    printf("%d\n", (((a1 << 2) + a2 + N) << 2) + 1);
    return 0;
}
