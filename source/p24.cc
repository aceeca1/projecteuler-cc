#include <cstdio>
#include <string>
using namespace std;

constexpr auto N = 1000000;

int main() {
    string a("0123456789");
    int factorial[10]{1};
    for (int i = 1; i < 10; ++i) factorial[i] = factorial[i - 1] * i;
    string answer;
    int k = N - 1;
    while (a.size()) {
        int m = factorial[a.size() - 1];
        int i = k / m;
        k %= m;
        answer += a[i];
        a.erase(i, 1);
    }
    printf("%s\n", answer.c_str());
    return 0;
}
