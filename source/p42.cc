#include <cstdio>
#include "../include/polygonal.h"
#include "../include/utility.h"
using namespace std;

int main() {
    auto in = fopen("../data/input42.txt", "r");
    const auto &a = readQuoteStrings<80>(in);
    fclose(in);
    int answer = 0;
    for (const auto &i : a) {
        int s = 0;
        for (char j : i) s += j - 'A' + 1;
        if (isPolygonal(3, s)) ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
