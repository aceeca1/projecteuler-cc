#include <cstdio>
using namespace std;

constexpr int N = 12000;

struct Fraction {
    int numerator, denominator;
};

int fractionsBetween(Fraction lower, Fraction upper) {
    Fraction middle{lower.numerator + upper.numerator,
                    lower.denominator + upper.denominator};
    if (N < middle.denominator) return 0;
    int answer1 = fractionsBetween(lower, middle);
    int answer2 = fractionsBetween(middle, upper);
    return answer1 + answer2 + 1;
}

int main() {
    printf("%d\n", fractionsBetween(Fraction{1, 2}, Fraction{1, 3}));
    return 0;
}
