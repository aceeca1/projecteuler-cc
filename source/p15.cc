#include <cstdio>
using namespace std;

template <class T>
T choose(int n, int k) {
    T answer(1);
    for (int i = 1; i <= k; ++i) answer = answer * (n - i + 1) / i;
    return answer;
}

int main() {
    printf("%lld\n", choose<long long>(40, 20));
    return 0;
}
