#include <algorithm>
#include <cstdio>
#include "../include/utility.h"
using namespace std;

int main() {
    auto in = fopen("../data/input22.txt", "r");
    auto &&a = readQuoteStrings<80>(in);
    fclose(in);
    sort(a.begin(), a.end());
    int answer = 0;
    for (int i = 0; i < (int)a.size(); ++i) {
        int answer1 = 0;
        for (char j : a[i]) answer1 += j - 'A' + 1;
        answer += answer1 * (i + 1);
    }
    printf("%d\n", answer);
    return 0;
}
