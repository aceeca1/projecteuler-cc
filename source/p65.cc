#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
#include <vector>
using namespace std;

constexpr int N = 100;

int main() {
    vector<int> a{2};
    for (int i = 2;; i += 2) {
        a.emplace_back(1);
        a.emplace_back(i);
        a.emplace_back(1);
        if (a.size() >= N) break;
    }
    NTL::ZZ numerator(NTL::INIT_VAL, 1), denominator(NTL::INIT_VAL, 0);
    for (int i = N - 1; i >= 0; --i) {
        NTL::ZZ numeratorLast(numerator);
        numerator *= a[i];
        numerator += denominator;
        denominator = move(numeratorLast);
    }
    stringstream ss;
    ss << numerator;
    int answer = 0;
    for (char i : ss.str()) answer += i - '0';
    printf("%d\n", answer);
    return 0;
}
