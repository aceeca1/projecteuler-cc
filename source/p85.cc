#include <climits>
#include <cmath>
#include <cstdio>
#include "../include/utility.h"
using namespace std;

int main() {
    int minDelta = INT_MAX, arg = -1, a1 = 1, a2 = 2000;
    while (a2) {
        int segment1 = a1 * (a1 + 1) >> 1;
        int segment2 = a2 * (a2 + 1) >> 1;
        int square = segment1 * segment2;
        assignMin(minDelta, abs(square - 2000000), arg, a1 * a2);
        square < 2000000 ? ++a1 : --a2;
    }
    printf("%d\n", arg);
}
