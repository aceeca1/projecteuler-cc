#include <algorithm>
#include <cmath>
#include <cstdio>
using namespace std;

int cuboids(int m) {
    int mSquare = m * m, answer = 0;
    for (int p = m + m; p >= 2; --p) {
        int q = p * p + mSquare;
        int sqrtQ = sqrt(q);
        if (sqrtQ * sqrtQ != q) continue;
        answer += (p >> 1) - max(p - m, 1) + 1;
    }
    return answer;
}

int main() {
    int solutions = 0, m = 0;
    while (solutions <= 1000000) solutions += cuboids(++m);
    printf("%d\n", m);
    return 0;
}
