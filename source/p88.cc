#include <climits>
#include <cstdio>
#include <unordered_set>
#include <vector>
#include "../include/utility.h"
using namespace std;

constexpr int N = 12000;

class ProductSumNumbers {
    void search(int sum, int product, int currentSize, int current) {
        int currentSum = sum + current;
        int currentProduct = product * current;
        int fullSize = currentSize + (currentProduct - currentSum);
        if (N < fullSize) return;
        if (currentSize == 1 && N < current * current) return;
        assignMin(answer[fullSize], currentProduct);
        search(currentSum, currentProduct, currentSize + 1, current);
        search(sum, product, currentSize, current + 1);
    }

   public:
    vector<int> answer;

    ProductSumNumbers() : answer(N + 1, INT_MAX) { search(0, 1, 1, 2); }
};

int main() {
    unordered_set<int> u;
    auto a = ProductSumNumbers().answer;
    for (int i = 2; i <= N; ++i) u.emplace(a[i]);
    int answer = 0;
    for (int i : u) answer += i;
    printf("%d\n", answer);
    return 0;
}
