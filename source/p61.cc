#include <algorithm>
#include <cstdio>
#include <vector>
#include "../include/polygonal.h"
using namespace std;

class CyclicalFigurateNumbers {
    vector<int> polygonal[6];
    int represent[6]{5, 0, 1, 2, 3, 4}, num[6];

    bool search(int k) {
        if (k == 6) return num[0] / 100 == num[5] % 100;
        for (int i : polygonal[represent[k]]) {
            if (k && i / 100 != num[k - 1] % 100) continue;
            num[k] = i;
            if (search(k + 1)) return true;
        }
        return false;
    }

   public:
    CyclicalFigurateNumbers() {
        for (int i = 3; i <= 8; ++i)
            for (int j = 1000; j < 10000; ++j)
                if (isPolygonal(i, j)) polygonal[i - 3].emplace_back(j);
        while (!search(0))
            next_permutation(begin(represent) + 1, end(represent));
    }

    int sum() const {
        return num[0] + num[1] + num[2] + num[3] + num[4] + num[5];
    }
};

int main() {
    printf("%d\n", CyclicalFigurateNumbers().sum());
    return 0;
}
