#include <NTL/ZZ.h>
#include <cstdio>
#include <functional>
#include <unordered_set>
#include <vector>
using namespace std;

constexpr int N = 50000000;

template <class Func>
vector<int> primeMap(Func&& f) {
    vector<int> answer;
    NTL::PrimeSeq s;
    while (true) {
        int fp = f(s.next());
        if (N <= fp) return answer;
        answer.emplace_back(fp);
    }
}

int main() {
    auto p2 = primeMap([](int n) { return n * n; });
    auto p3 = primeMap([](int n) { return n * n * n; });
    auto p4 = primeMap([](int n) { return n * n * n * n; });
    unordered_set<int> u;
    for (int i2 : p2)
        for (int i3 : p3)
            for (int i4 : p4) {
                int i = i2 + i3 + i4;
                if (N <= i) break;
                u.emplace(i);
            }
    printf("%d\n", (int)u.size());
    return 0;
}
