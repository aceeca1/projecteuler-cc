#include <cstdio>
#include <string>
#include "../include/utility.h"
using namespace std;

bool hasOneToNine(const string &s) {
    int ss = 0;
    for (char i : s) ss |= 1 << (i - '0');
    return ss == 0x3fe;
}

int main() {
    string answer;
    for (int i = 1; i < 10000; ++i) {
        string s = to_string(i);
        for (int j = i + i;; j += i)
            if (s.size() < 9)
                s += to_string(j);
            else
                break;
        if (s.size() == 9 && hasOneToNine(s)) assignMax(answer, s);
    }
    printf("%s\n", answer.c_str());
}
