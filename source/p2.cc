#include <cstdio>
#include "../include/fibonacci.h"
using namespace std;

int main() {
    int answer = 0;
    for (FibonacciIt<int> fib(1, 2); *fib < 4000000; ++fib)
        if (!(*fib & 1)) answer += *fib;
    printf("%d\n", answer);
    return 0;
}
