#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

constexpr auto N = 100;
constexpr auto M = 50;

int main() {
    NTL::ZZ answer;
    auto in = fopen("../data/input13.txt", "r");
    for (int i = 0; i < N; ++i) {
        char s[M + 2];
        fgets(s, M + 2, in);
        s[M] = 0;
        answer += NTL::ZZ(NTL::INIT_VAL, s);
    }
    fclose(in);
    stringstream strs;
    strs << answer;
    printf("%s\n", strs.str().substr(0, 10).c_str());
    return 0;
}
