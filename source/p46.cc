#include <NTL/ZZ.h>
#include <cstdio>
#include <iterator>
#include <queue>
using namespace std;

using Seq = iterator<forward_iterator_tag, int>;

class TwoTimesSquareSeq : public Seq {
    int n, v;

   public:
    TwoTimesSquareSeq() : n(0), v(0) {}
    int operator*() const { return v; }
    TwoTimesSquareSeq &operator++() {
        ++n;
        v = n * n << 1;
        return *this;
    }
};

class PrimeSeq : public Seq {
    NTL::PrimeSeq s;
    int v;

   public:
    PrimeSeq() { v = s.next(); }
    int operator*() const { return v; }
    PrimeSeq &operator++() {
        v = s.next();
        return *this;
    }
};

template <class Seq1, class Seq2>
class SumSeq : public Seq {
    class Seq1Plus : public Seq {
        Seq1 seq1;

       public:
        int plus;
        Seq1Plus(int plus_) : plus(plus_) {}
        int operator*() const { return *seq1 + plus; }
        Seq1Plus &operator++() {
            ++seq1;
            return *this;
        }
        bool operator<(const Seq1Plus &rhs) const { return **this > *rhs; }
    };

    priority_queue<Seq1Plus> q;
    Seq2 seq2;

   public:
    SumSeq() { q.emplace(*seq2); }
    int operator*() const { return *q.top(); }
    SumSeq &operator++() {
        Seq1Plus qTop(move(q.top()));
        q.pop();
        if (qTop.plus == *seq2) q.emplace(*++seq2);
        ++qTop;
        q.emplace(move(qTop));
        return *this;
    }
};

class OddSeq : public Seq {
    int v;

   public:
    OddSeq() : v(3) {}
    int operator*() const { return v; }
    OddSeq &operator++() {
        v += 2;
        return *this;
    }
};

template <class Seq1, class Seq2>
class DifferenceSeq : public Seq {
    Seq1 seq1;
    Seq2 seq2;

    void adjust() {
        while (true) {
            if (*seq1 < *seq2)
                break;
            else if (*seq1 == *seq2)
                ++seq1;
            else
                ++seq2;
        }
    }

   public:
    DifferenceSeq() { adjust(); }
    int operator*() const { return *seq1; }
    DifferenceSeq &operator++() {
        ++seq1;
        adjust();
        return *this;
    }
};

int main() {
    DifferenceSeq<OddSeq, SumSeq<TwoTimesSquareSeq, PrimeSeq>> seq;
    printf("%d\n", *seq);
    return 0;
}
