#include <algorithm>
#include <cstdio>
#include "../include/divisor.h"
using namespace std;

int solve() {
    int k = 0;
    for (int i = 2;; ++i) {
        auto &&a = factorize(i);
        int z = unique(a.begin(), a.end()) - a.begin();
        if (z == 4)
            ++k;
        else
            k = 0;
        if (k == 4) return i - 3;
    }
}

int main() {
    printf("%d\n", solve());
    return 0;
}
