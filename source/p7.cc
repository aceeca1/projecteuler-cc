#include <NTL/ZZ.h>
#include <cstdio>
using namespace std;

constexpr auto N = 10001;

int main() {
    NTL::PrimeSeq s;
    for (int i = 1; i < N; ++i) s.next();
    printf("%ld\n", s.next());
    return 0;
}
