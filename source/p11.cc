#include <cstdio>
#include <tuple>
#include <vector>
#include "../include/utility.h"
using namespace std;

constexpr auto N = 20;
constexpr auto M = 4;

class FindMaxProduct {
    vector<tuple<int, int>> directions{{0, 1}, {1, 0}, {1, 1}, {1, -1}};
    const vector<vector<int>> &a;

   public:
    int answer = 0;

    int along(int x1, int x2, const tuple<int, int> &direction) {
        int answer = 1;
        for (int i = 0; i < M; ++i) {
            if (!(0 <= x1 && x1 < N)) return 0;
            if (!(0 <= x2 && x2 < N)) return 0;
            answer *= a[x1][x2];
            x1 += get<0>(direction);
            x2 += get<1>(direction);
        }
        return answer;
    }

    FindMaxProduct(const vector<vector<int>> &a_) : a(a_) {
        for (int i1 = 0; i1 < N; ++i1)
            for (int i2 = 0; i2 < N; ++i2)
                for (const auto &j : directions)
                    assignMax(answer, along(i1, i2, j));
    }
};

int main() {
    auto in = fopen("../data/input11.txt", "r");
    vector<vector<int>> a(N, vector<int>(N));
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; ++j) fscanf(in, "%d", &a[i][j]);
    fclose(in);
    printf("%d\n", FindMaxProduct(a).answer);
    return 0;
}
