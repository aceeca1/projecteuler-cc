#include <NTL/ZZ.h>
#include <cstdio>
using namespace std;

class TruncatablePrime {
    void put(int m) {
        for (int i : {1, 3, 7, 9}) {
            int mi = m * 10 + i;
            if (NTL::ProbPrime(mi)) put(mi);
        }
        if (m < 10) return;
        for (int i = 10; i <= m; i *= 10) {
            int mi = m % i;
            if (!NTL::ProbPrime(mi)) return;
        }
        sum += m;
    }

   public:
    int sum;
    TruncatablePrime() {
        put(2);
        put(3);
        put(5);
        put(7);
    }
};

int main() {
    printf("%d\n", TruncatablePrime().sum);
    return 0;
}
