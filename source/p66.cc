#include <NTL/ZZ.h>
#include <cmath>
#include <cstdio>
#include "../include/utility.h"
using namespace std;

struct PellEquation {
    bool isSolution() const { return p * p - q * q * n == 1; }

   public:
    int n;
    NTL::ZZ p, q;

    PellEquation(int n_) : n(n_) {
        int sqrtN = sqrt(n);
        if (sqrtN * sqrtN == n) {
            p = 1, q = 0;
            return;
        }
        p = sqrtN, q = 1;
        if (isSolution()) return;
        NTL::ZZ p0(p), q0(q);
        int u1 = sqrtN, v1 = n - sqrtN * sqrtN, a1 = (sqrtN + sqrtN) / v1;
        auto &p1 = p, &q1 = q;
        p1 = p0 * a1 + 1, q1 = a1;
        while (!isSolution()) {
            int u2 = a1 * v1 - u1;
            int v2 = (n - u2 * u2) / v1;
            int a2 = (sqrtN + u2) / v2;
            auto p2 = a2 * p1 + p0;
            auto q2 = a2 * q1 + q0;
            p0 = p1, q0 = q1, u1 = u2, v1 = v2, a1 = a2, p1 = p2, q1 = q2;
        }
    }
};

int main() {
    NTL::ZZ maxP;
    int arg = -1;
    for (int i = 1; i <= 1000; ++i) assignMax(maxP, PellEquation(i).p, arg, i);
    printf("%d\n", arg);
    return 0;
}
