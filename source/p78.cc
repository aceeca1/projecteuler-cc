#include <cstdio>
#include <vector>
using namespace std;

vector<int> waysAsCoinPartition(int n) {
    vector<int> answer(n + 1);
    answer[0] = 1;
    for (int i = 1; i <= n; ++i)
        for (int j = i; j <= n; ++j)
            answer[j] = (answer[j] + answer[j - i]) % 1000000;
    return answer;
}

bool hasZero(const vector<int>& v, int* index) {
    for (*index = 0; *index < (int)v.size(); ++*index)
        if (v[*index] == 0) return true;
    return false;
}

int main() {
    int n = 1, answer;
    while (!hasZero(waysAsCoinPartition(n), &answer)) n += n;
    printf("%d\n", answer);
    return 0;
}
