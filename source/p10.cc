#include <NTL/ZZ.h>
#include <cstdio>
using namespace std;

constexpr auto N = 2000000;

int main() {
    NTL::PrimeSeq s;
    long long answer = 0;
    while (true) {
        auto i = s.next();
        if (N <= i) break;
        answer += i;
    }
    printf("%lld\n", answer);
}
