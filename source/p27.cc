#include <NTL/ZZ.h>
#include <cstdio>
#include "../include/utility.h"
using namespace std;

constexpr auto N = 1000;

int main() {
    int maxPrimes = 0, arg = -1;
    for (int a = -(N - 1); a <= N - 1; ++a)
        for (int b = -N; b <= N; ++b) {
            int n = 0;
            while (NTL::ProbPrime((n + a) * n + b)) ++n;
            assignMax(maxPrimes, n, arg, a * b);
        }
    printf("%d\n", arg);
    return 0;
}
