#include <cstdio>
#include "../include/binary.h"
#include "../include/palindrome.h"
using namespace std;

constexpr auto N = 1000000;

int main() {
    int answer = 0;
    foreachPalindromeBelow(N, [&answer](int n) {
        if (isPalindrome(toBinaryString(n))) answer += n;
    });
    printf("%d\n", answer);
    return 0;
}
