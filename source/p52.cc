#include <cstdint>
#include <cstdio>
#include <string>
#include "../include/digit.h"
using namespace std;

bool multiplesArePermutation(int n) {
    auto c2 = countDigits(n + n);
    for (int i = 3; i <= 6; ++i)
        if (c2 != countDigits(n * i)) return false;
    return true;
}

int main() {
    int answer = 1;
    while (!multiplesArePermutation(answer)) ++answer;
    printf("%d\n", answer);
    return 0;
}
