#include <cstdio>
#include "../include/polygonal.h"
using namespace std;

int solve() {
    for (int i = 144;; ++i) {
        long long a = getPolygonal(6, i);
        if (isPolygonal(5, a) && isPolygonal(3, a)) return a;
    }
}

int main() { printf("%d\n", solve()); }
