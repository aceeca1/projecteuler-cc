#include <cmath>
#include <cstdio>
#include <unordered_set>
#include <vector>
#include "../include/utility.h"
using namespace std;

class ArithmeticLengthFromOne {
    vector<double> num;
    unordered_set<int> results;

    template <class Func>
    void forEachRemoving(Func&& f) {
        for (int i = 0; i < (int)num.size(); ++i) {
            double numI = num[i];
            num[i] = num.back();
            num.pop_back();
            f(numI);
            num.emplace_back(num[i]);
            num[i] = numI;
        }
    }

   public:
    int answer = 0;

    void search() {
        if (num.size() == 1) {
            auto roundNum0 = round(num[0]);
            if (1.0 <= roundNum0 && num[0] - roundNum0 < 1e-9)
                results.emplace((int)roundNum0);
        }
        forEachRemoving([&](double i1) {
            forEachRemoving([&](double i2) {
                for (double j : {i1 + i2, i1 - i2, i2 - i1,  //
                                 i1 * i2, i1 / i2, i2 / i1}) {
                    num.emplace_back(j);
                    search();
                    num.pop_back();
                }
            });
        });
    }

    ArithmeticLengthFromOne(const vector<int>& num_) : num(num_.size()) {
        for (int i = 0; i < (int)num_.size(); ++i) num[i] = num_[i];
        search();
        while (results.count(answer + 1)) ++answer;
        results.clear();
    }
};

class TenChooseFour {
    vector<int> num;

   public:
    template <class Func>
    void forEach(Func&& f, int n = 1) {
        if (num.size() == 4) return f(num);
        if (n == 10) return;
        num.emplace_back(n);
        forEach(f, n + 1);
        num.pop_back();
        forEach(f, n + 1);
    }
};

int main() {
    int maxLength = 0;
    vector<int> arg;
    TenChooseFour().forEach([&](const vector<int>& num) {
        assignMax(maxLength, ArithmeticLengthFromOne(num).answer, arg, num);
    });
    printf("%d%d%d%d\n", arg[0], arg[1], arg[2], arg[3]);
    return 0;
}
