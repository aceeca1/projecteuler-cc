#include <cstdio>
#include "../include/divisor.h"
using namespace std;

bool isAmicable(int i) {
    int u = Divisor(i).sum() - i;
    if (i == u) return false;
    int v = Divisor(u).sum() - u;
    return i == v;
}

int main() {
    int answer = 0;
    for (int i = 1; i <= 10000; ++i)
        if (isAmicable(i)) answer += i;
    printf("%d\n", answer);
    return 0;
}
