#include <NTL/ZZ.h>
#include <cstdio>
using namespace std;

class Fraction {
   public:
    int numerator, denominator;

    Fraction &operator*=(const Fraction &rhs) {
        numerator *= rhs.numerator;
        denominator *= rhs.denominator;
        return *this;
    }

    Fraction &simplify() {
        int gcd = NTL::GCD(numerator, denominator);
        numerator /= gcd;
        denominator /= gcd;
        return *this;
    }
};

template <class Func>
static void forAllSolutions(Func &&f) {
    for (int n1 = 1; n1 <= 9; ++n1) {
        int m0 = n1;
        for (int n0 = 0; n0 <= 9; ++n0) {
            int n = n1 * 10 + n0;
            for (int m1 = 1; m1 <= 9; ++m1) {
                int m = m1 * 10 + m0;
                if (m != n && m * n0 == n * m1) f(Fraction{m, n});
            }
        }
    }
}

int main() {
    Fraction answer{1, 1};
    forAllSolutions([&answer](const Fraction &i) { answer *= i; });
    printf("%d\n", answer.simplify().denominator);
    return 0;
}
