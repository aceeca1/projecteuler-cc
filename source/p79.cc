#include <climits>
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

class KeySearcher {
   public:
    vector<string> attempts;
    string key, bestKey;

    bool succeed(const vector<int>& positions) const {
        for (int i = 0; i < (int)attempts.size(); ++i)
            if (positions[i] != (int)attempts[i].size()) return false;
        return true;
    }

    void search(vector<int>&& positions) {
        if (bestKey.size() && bestKey.size() <= key.size()) return;
        if (succeed(positions)) {
            bestKey = key;
            return;
        }
        bool visited[10]{};
        for (int i = 0; i < (int)attempts.size(); ++i) {
            if (positions[i] == (int)attempts[i].size()) continue;
            char c = attempts[i][positions[i]];
            if (visited[c - '0']) continue;
            visited[c - '0'] = true;
            vector<int> newPositions(positions);
            for (int j = 0; j < (int)attempts.size(); ++j)
                if (newPositions[j] != (int)attempts[j].size())
                    if (attempts[j][newPositions[j]] == c) ++newPositions[j];
            key += c;
            search(move(newPositions));
            key.pop_back();
        }
    }

    void search() { search(vector<int>(attempts.size())); }
};

int main() {
    KeySearcher searcher;
    auto in = fopen("../data/input79.txt", "r");
    while (true) {
        char s[4];
        if (fscanf(in, "%s", s) < 1) break;
        searcher.attempts.emplace_back(s);
    }
    fclose(in);
    searcher.search();
    printf("%s\n", searcher.bestKey.c_str());
    return 0;
}
