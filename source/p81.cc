#include <climits>
#include <cstdio>
#include <vector>
#include "../include/utility.h"
using namespace std;

constexpr int N = 80;

int main() {
    vector<vector<int>> a(N, vector<int>(N));
    auto in = fopen("../data/input81.txt", "r");
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; ++j) fscanf(in, "%d,", &a[i][j]);
    fclose(in);
    vector<int> b(N);
    for (int i = 1; i < N; ++i) b[i] = INT_MAX;
    for (auto& ai : a) {
        b[0] += ai[0];
        for (int j = 1; j < N; ++j) {
            assignMin(b[j], b[j - 1]);
            b[j] += ai[j];
        }
    }
    printf("%d\n", b[N - 1]);
    return 0;
}
