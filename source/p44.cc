#include <cstdio>
#include "../include/divisor.h"
#include "../include/polygonal.h"
using namespace std;

/* {a, b, a + b, a - b} are pentagonal. Let c = a - b, and they becomes
{b, c, c + b, c + 2b}. Let c + b = 3q(q - 1) / 2, b = 3s(s - 1) / 2.
We have (3q + 3s - 1)(q - s) = 2c. */

int solve() {
    for (int i = 2;; ++i) {
        int c = getPolygonal(5, i);
        int cTimes2 = c + c;
        for (int k1 : Divisor(cTimes2)) {
            int k2 = k1 + 1;
            if (k2 % 3) continue;
            int k3 = k2 / 3;
            int k4 = cTimes2 / k1;
            if (k3 <= k4) continue;
            int k5 = k3 + k4;
            if (k5 & 1) continue;
            int q = k5 >> 1;
            int s = k3 - q;
            int a = getPolygonal(5, q);
            int b = getPolygonal(5, s);
            if (isPolygonal(5, a + b)) return c;
        }
    }
}

int main() { printf("%d\n", solve()); }
