#include <NTL/ZZ.h>
#include <cstdio>
using namespace std;

int main() {
    long answer = 1;
    for (int i = 2; i <= 20; ++i) answer *= i / NTL::GCD(answer, i);
    printf("%ld\n", answer);
    return 0;
}
