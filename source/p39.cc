#include <cstdio>
#include "../include/utility.h"
using namespace std;

constexpr auto N = 1000;

int main() {
    int maxSolution = 0, arg = -1;
    for (int p = 1; p <= N; ++p) {
        int solution = 0;
        for (int a = 1; a < p; ++a) {
            int aSquare = a * a;
            int bPlusC = p - a;
            if (aSquare % bPlusC) continue;
            int cMinusB = aSquare / bPlusC;
            int bTimes2 = bPlusC - cMinusB;
            if (bTimes2 & 1) continue;
            int b = bTimes2 >> 1;
            if (a < b) ++solution;
        }
        assignMax(maxSolution, solution, arg, p);
    }
    printf("%d\n", arg);
}
