#include <climits>
#include <cstdio>
#include <string>
#include <vector>
#include "../include/binary.h"
#include "../include/utility.h"
using namespace std;

class Sudoku {
   public:
    using Board = vector<int>;

    Sudoku() {
        for (int iX = 0; iX < 9; ++iX)
            for (int iY = 0; iY < 9; ++iY)
                for (int jX = 0; jX < 9; ++jX)
                    for (int jY = 0; jY < 9; ++jY)
                        if (adjacent(iX, iY, jX, jY))
                            e[iX * 9 + iY].emplace_back(jX * 9 + jY);
    }

    bool solve(Board* board) {
        for (int& i : *board) i = i ? 1 << i : 0x3fe;
        for (int i = 0; i < 81; ++i)
            if (isSingleBit(board->at(i))) let(board, i, board->at(i));
        if (solveInternal(board)) {
            for (int& i : *board) i = whichBit(i);
            return true;
        }
        return false;
    }

   private:
    vector<int> e[81];

    void let(Board* board, int position, int value) {
        board->at(position) = value;
        for (int i : e[position]) {
            auto& boardI = board->at(i);
            bool wasSingle = isSingleBit(boardI);
            boardI &= ~value;
            if (boardI == 0) return;
            if (!wasSingle && isSingleBit(boardI)) let(board, i, boardI);
        }
    }

    bool solveInternal(Board* board) {
        int minFork = INT_MAX, arg = -1;
        for (int i = 0; i < 81; ++i) {
            int count = countLowest14Bits(board->at(i));
            if (count != 1) assignMin(minFork, count, arg, i);
        }
        if (minFork == INT_MAX) return true;
        int s = board->at(arg);
        while (true) {
            int i = lowestBit(s);
            if (i == 0) return false;
            Board newBoard(*board);
            let(&newBoard, arg, i);
            if (solveInternal(&newBoard)) {
                *board = std::move(newBoard);
                return true;
            }
            s -= i;
        }
    }

    static bool adjacent(int x1, int y1, int x2, int y2) {
        if (x1 == x2 && y1 == y2) return false;
        if (x1 == x2 || y1 == y2) return true;
        return x1 / 3 == x2 / 3 && y1 / 3 == y2 / 3;
    }

    static int lowestBit(int n) { return n & -n; }
    static bool isSingleBit(int n) { return n == lowestBit(n); }
};

int main() {
    int answer = 0;
    Sudoku sudoku;
    auto in = fopen("../data/input96.txt", "r");
    while (true) {
        if (fscanf(in, " Grid%*d") == EOF) break;
        Sudoku::Board board;
        for (int i = 0; i < 9; ++i) {
            char s[10];
            fscanf(in, "%s", s);
            for (int j = 0; j < 9; ++j) board.emplace_back(s[j] - '0');
        }
        sudoku.solve(&board);
        answer += (board[0] * 10 + board[1]) * 10 + board[2];
    }
    fclose(in);
    printf("%d\n", answer);
    return 0;
}
