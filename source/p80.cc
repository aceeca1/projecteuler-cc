#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

int main() {
    auto multiplier = NTL::power_ZZ(10, 200);
    int answer = 0;
    for (int i = 1; i <= 100; ++i) {
        int sqrtI = NTL::SqrRoot(i);
        if (sqrtI * sqrtI == i) continue;
        stringstream ss;
        ss << NTL::SqrRoot(i * multiplier);
        string squareRoot = ss.str();
        squareRoot.resize(100);
        for (char j : squareRoot) answer += j - '0';
    }
    printf("%d\n", answer);
    return 0;
}
