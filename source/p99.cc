#include <cmath>
#include <cstdio>
#include "../include/utility.h"
using namespace std;

int main() {
    double maxValue = 0;
    int arg = -1;
    auto in = fopen("../data/input99.txt", "r");
    for (int i = 1; ; ++i) {
        int base, exponent;
        if (fscanf(in, "%d,%d", &base, &exponent) < 2) break;
        assignMax(maxValue, log(base) * exponent, arg, i);
    }
    fclose(in);
    printf("%d\n", arg);
    return 0;
}
