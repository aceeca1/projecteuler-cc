#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

int main() {
    int answer = 0;
    for (int i = 1; i <= 100; ++i) {
        NTL::ZZ power(NTL::INIT_VAL, i);
        for (int j = 1; j <= 100; ++j) {
            stringstream ss;
            ss << power;
            int digitSum = 0;
            for (char k : ss.str()) digitSum += k - '0';
            if (answer < digitSum) answer = digitSum;
            power *= i;
        }
    }
    printf("%d\n", answer);
    return 0;
}
