#include <NTL/ZZ.h>
#include <cstdio>
using namespace std;

int solve() {
    int s = 1, numPrime = 0, numAll = 1;
    for (int i = 2;; i += 2) {
        for (int j = 0; j < 4; ++j) {
            s += i;
            if (NTL::ProbPrime(s)) ++numPrime;
            ++numAll;
        }
        if (numPrime * 10 < numAll) return i + 1;
    }
}

int main() {
    printf("%d\n", solve());
    return 0;
}
