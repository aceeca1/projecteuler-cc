#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

class SqrtConvergents {
    NTL::ZZ n1{NTL::INIT_VAL, 3}, n2{NTL::INIT_VAL, 2};

   public:
    void next() {
        auto n2Last = n2;
        n2 += n1;
        n1 = move(n2Last += n2);
    }

    bool numeratorHasMoreDigits() {
        stringstream ss1, ss2;
        ss1 << n1;
        ss2 << n2;
        return ss1.str().size() > ss2.str().size();
    }
};

int main() {
    int answer = 0;
    SqrtConvergents sc;
    for (int i = 0; i < 1000; ++i) {
        if (sc.numeratorHasMoreDigits()) ++answer;
        sc.next();
    }
    printf("%d\n", answer);
    return 0;
}
