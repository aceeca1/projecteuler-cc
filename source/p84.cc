#include <algorithm>
#include <cstdio>
#include <vector>
#include "../include/matrix.h"
using namespace std;

int main() {
    vector<double> aheadProb(40);
    for (int i = 1; i <= 4; ++i)
        for (int j = 1; j <= 4; ++j) aheadProb[i + j] += 1.0 / 16;
    double threeDoubleJail = 3.0 / 1024;
    for (int i = 2; i <= 8; i += 2) aheadProb[i] -= threeDoubleJail;
    Matrix<double> transferProb(40, 40);
    for (int i = 0; i < 40; ++i) {
        for (int j = 0; j < 40; ++j) {
            int ahead = j - i;
            if (ahead < 0) ahead += 40;
            transferProb.data[i][j] = aheadProb[ahead];
        }
        transferProb.data[i][10] += threeDoubleJail * 4.0;
    }
    struct SpecialSquare {
        int number;
        vector<int> destination;
    } special[]{
        {30, vector<int>(16, 10)},
        {7, {0, 10, 11, 24, 39, 5, 15, 15, 12, 4}},
        {22, {0, 10, 11, 24, 39, 5, 25, 25, 28, 19}},
        {36, {0, 10, 11, 24, 39, 5, 5, 5, 12, 33}},
        {2, {0, 10}},
        {17, {0, 10}},
        {33, {0, 10}},
    };
    for (const auto& item : special) {
        for (int i = 0; i < 40; ++i) {
            double share = transferProb.data[i][item.number] * (1.0 / 16);
            transferProb.data[i][item.number] =
                share * (16 - item.destination.size());
            for (int j : item.destination) transferProb.data[i][j] += share;
        }
    }
    for (int i = 1; i <= 30; ++i) transferProb = transferProb * transferProb;
    vector<int> nth(40);
    for (int i = 0; i < 40; ++i) nth[i] = i;
    sort(nth.begin(), nth.end(), [&](int x1, int x2) {
        return transferProb.data[0][x1] > transferProb.data[0][x2];
    });
    printf("%d%d%d\n", nth[0], nth[1], nth[2]);
    return 0;
}
