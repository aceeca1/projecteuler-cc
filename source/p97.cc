#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

int main() {
    auto m = NTL::ZZ(NTL::INIT_VAL, "10000000000");
    auto n = NTL::PowerMod(NTL::ZZ(NTL::INIT_VAL, 2), 7830457, m);
    n = (n * 28433 + 1) % m;
    stringstream ss;
    ss << n;
    printf("%s\n", ss.str().c_str());
    return 0;
}
