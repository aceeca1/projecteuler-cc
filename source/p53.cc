#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int answer = 0;
    vector<double> c{};
    for (int i = 0; i <= 100; ++i) {
        c.emplace_back(1.0);
        for (int j = i - 1; j >= 0; --j) c[j] += c[j - 1];
        for (auto j : c)
            if (j > 1e6) ++answer;
    }
    printf("%d\n", answer);
    return 0;
}
