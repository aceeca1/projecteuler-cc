#include <NTL/ZZ.h>
#include <cstdio>
#include <sstream>
using namespace std;

int main() {
    int answer = 0;
    for (int i = 1; i <= 9; ++i) {
        NTL::ZZ product(NTL::INIT_VAL, i);
        for (int j = 1;; ++j) {
            stringstream ss;
            ss << product;
            int size = ss.str().size();
            if (size < j) break;
            if (size == j) ++answer;
            product *= i;
        }
    }
    printf("%d\n", answer);
    return 0;
}
